package password;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

/*
 * Author			: Derek Schunicke
 * Student Number	: 991295326 
 * 
*/

public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloWorld");
		assertTrue("Invalid password length", isValidLength);
	}
	
		
	@Test
	public void testIsValidLengthException() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello");
		assertFalse("Invalid password length", isValidLength);
	}
	

	@Test
	public void testIsValidLengthExceptionSpace() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello World");
		assertFalse("Invalid Password Character", isValidLength);
	}
	
	
	@Test
	public void testIsValidLengthExceptionNull() {
		boolean isValidLength = PasswordValidator.isValidLength(null);
		assertFalse("Invalid Password Length", isValidLength);
	} 	
	

	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello12");
		assertFalse("Invalid password length", isValidLength);
	}
	
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean isValidLength = PasswordValidator.isValidLength("Hello123");
		assertTrue("Invalid password length", isValidLength);
	}

	@Test
	public void testHasValidDigitCountRegular() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Hello123");
		assertTrue("Invalid password length", hasValidDigitCount);
	}
	
	@Test
	public void testHasValidDigitCountException() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Hello4#!@");
		assertFalse("Invalid characters", hasValidDigitCount);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryIn() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Hello12");
		assertTrue("Invalid characters", hasValidDigitCount);
	}
	
	@Test
	public void testHasValidDigitCountBoundaryOut() {
		boolean hasValidDigitCount = PasswordValidator.hasValidDigitCount("Hello1");
		assertFalse("Invalid characters", hasValidDigitCount);
	}

}
