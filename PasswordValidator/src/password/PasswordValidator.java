package password;

/*
 * Author			: Derek Schunicke
 * Student Number	: 991295326 
 * 
 * We assume spaces are not considered valid characters 
 * for the purpose of calculating length.
 * 
*/

public class PasswordValidator {

	private static int MIN_LENGTH = 8;
	
	public static boolean isValidLength(String password) {
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
	public static boolean hasValidDigitCount(String password) {
		int totalDigits = 0;
		for(char c : password.toCharArray()) {
			if(Character.isDigit(c))
				totalDigits++;
		}
		return totalDigits >= 2;
	}
	
}
